import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../_services/account.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  @Output() cancelLogin = new EventEmitter();
  model: any = {};

  constructor(
    private accountService: AccountService,
    private toastr: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
  }

  //call to login service and verify credentials
  login() {
    this.spinner.show();
    this.accountService.login(this.model).subscribe(
      (response) => {
        this.spinner.hide();
        this.router.navigateByUrl('/accountbalance');
      },
      (error) => {
        this.spinner.hide();
        this.toastr.error(error.error);
      }
    );
  }

  //handles cancel button in login form
  cancel() {
    this.cancelLogin.emit(false);
  }
}

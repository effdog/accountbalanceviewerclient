import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { User } from '../_models/user';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  loginMode = false;

  constructor(private accountService : AccountService,
    private router : Router) {}

  ngOnInit(): void {
    this.isUserLoggedIn()
  }

//show and hide the login form
  loginToggle() {
    this.loginMode = !this.loginMode;
  }

  //cancel button of login form
  cancenLoginMode(event: boolean) {
    this.loginMode = event;
  }

  //
  isUserLoggedIn(){
  let currentUser: User;
  this.accountService.currentUser$
    .pipe(take(1))
    .subscribe((user) => (currentUser = user));

    if(currentUser){
      this.router.navigate(['/accountbalance'])
    }
  }
}

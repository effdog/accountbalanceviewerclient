import { stripGeneratedFileSuffix } from '@angular/compiler/src/aot/util';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { biglistofyears } from '../Shared/years';
import { Balances } from '../_models/balances';
import { User } from '../_models/user';
import { AccountService } from '../_services/account.service';
import { BalanceService } from '../_services/balance.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css'],
})
export class BalanceComponent implements OnInit {
  month: any;
  p: number = 1;
  fileContent: any = {};
  tableData: Balances[] = [];
  yearParam: any;
  isAdmin: boolean;

  templatePath = environment.excelTemplate;
  yearList: any[] = biglistofyears;

  constructor(
    private balanceService: BalanceService,
    private toastr: ToastrService,
    private accountService: AccountService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.yearParam = 2021;
    this.loadTable();
    this.getRole();
  }

  //read the uploaded excel file and covert it to base64 string
  readFile(event) {
    const file = event.target.files[0];
    const fileName = event.target.files[0].name;
    const reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = () => {
      this.fileContent.fileContent = reader.result;
      this.fileContent.fileName = fileName;
    };
  }

  //send the base64 string of the uploaded document to the server
  upload() {
    this.balanceService.uploadFiles(this.fileContent).subscribe(
      (response) => {
        this.spinner.hide();
        this.toastr.success('Success');
        this.loadTable();
      },
      (error) => {
        this.toastr.error(error.error);
        window.location.reload();
      }
    );
  }

  //load the data into the table
  loadTable() {
    this.balanceService.getBalance(this.yearParam).subscribe((response) => {
      console.log(response);
      this.tableData = response;
    });
  }

  //reset dropdown fileter of the table to 2021
  resetFilters() {
    this.yearParam = '2021';
    this.loadTable();
  }

  //get the current user role of logged in user
  getRole() {
    let currentUser: User;
    this.accountService.currentUser$
      .pipe(take(1))
      .subscribe((user) => (currentUser = user));

    if (currentUser.roles.includes('Admin')) {
      this.isAdmin = true;
    }
  }
}

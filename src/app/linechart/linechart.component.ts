import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { BalanceComponent } from '../balance/balance.component';
import { biglistofyears } from '../Shared/years';
import { BalanceService } from '../_services/balance.service';

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.css'],
})
export class LinechartComponent implements OnInit {
  chart: any = [];
  yearList: any[] = biglistofyears;
  yearParam: any;

  constructor(private balanceserive: BalanceService) {}

  ngOnInit(): void {
    this.yearParam = 2021;
    this.loadChart();
  }

  //load the linechart using chartJS library
  loadChart() {
    this.balanceserive.getBalance(this.yearParam).subscribe((res) => {
      let Rnd = res.map((res) => res.rnD);
      let canteen = res.map((res) => res.canteen);
      let ceoCar = res.map((res) => res.ceoCar);
      let marketing = res.map((res) => res.marketing);
      let parkingFees = res.map((res) => res.parkingFees);

      let month = res.map((res) => res.month);

      this.chart = new Chart('canvas', {
        type: 'line',
        data: {
          labels: month,
          datasets: [
            {
              data: Rnd,
              fill: true,
              backgroundColor: 'rgba(255, 99, 71, 0.1)',
              borderColor: 'rgba(255, 99, 71, 0.1)',
              label: 'R&D',
            },
            {
              data: canteen,
              fill: true,
              backgroundColor: 'rgba(60, 179, 113, 0.2)',
              borderColor: 'rgba(60, 179, 113, 0.5)',
              label: 'Canteen',
            },
            {
              data: ceoCar,
              fill: true,
              backgroundColor: 'rgba(0, 0, 255, 0.2)',
              borderColor: 'rgba(0, 0, 255, 0.5)',
              label: "Ceo's Car",
            },
            {
              data: marketing,
              fill: true,
              backgroundColor: 'rgba(106, 90, 205, 0.2)',
              borderColor: 'rgba(106, 90, 205, 0.5)',
              label: 'Marketing',
            },
            {
              data: parkingFees,
              fill: true,
              backgroundColor: 'rgba(255, 165, 0, 0.2)',
              borderColor: 'rgba(255, 165, 0, 0.5)',
              label: 'Parking Fees',
            },
          ],
        },
        options: {
          legend: {
            display: true,
            position: 'bottom',
          },
          scales: {
            xAxes: [
              {
                display: true,
              },
            ],
            yAxes: [
              {
                display: true,
              },
            ],
          },
        },
      });
    });
  }

  //to handle reset fileter button onclick
  resetFilters() {
    this.yearParam = '2021';
    this.loadChart();
  }
}

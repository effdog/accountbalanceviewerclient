import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Balances } from '../_models/balances';

@Injectable({
  providedIn: 'root',
})
export class BalanceService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  //upload excel file and save data to DB
  uploadFiles(model: any): Observable<any> {
    return this.http.post(this.baseUrl + 'balances/uploadexcel', model);
  }

  //get data for datatable and for the chart
  getBalance(yearParam?): Observable<any> {
    const httpOptions = {
      params: new HttpParams().set('year', yearParam),
    };
    return this.http.get<Balances[]>(
      this.baseUrl + 'balances/getbalances',
      httpOptions
    );
  }
}

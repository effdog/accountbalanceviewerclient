import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BalanceComponent } from './balance/balance.component';
import { HomeComponent } from './home/home.component';
import { LinechartComponent } from './linechart/linechart.component';
import { AuthGuard } from './_guards/auth.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'accountbalance',
        component: BalanceComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'charts',
        component: LinechartComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  { path: '**', component: BalanceComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

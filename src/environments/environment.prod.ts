export const environment = {
  production: true,
  apiUrl: 'https://accbv-api.azurewebsites.net/api/',
  excelTemplate : "https://abvfilestorage.blob.core.windows.net/statictemplates/Template.xlsx"
};
